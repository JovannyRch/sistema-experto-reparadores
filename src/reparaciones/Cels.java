/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reparaciones;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Cels extends Agent{
    Socket conexion;
    ServerSocket servidor;
    DataOutputStream salidaCliente;
    ObjectInputStream entrada;
    ObjectOutputStream salida;
    Ventana2 vnt;
    
    class Reparar extends SimpleBehaviour{
        @Override
        public void action(){
             
            while(true){
                try {
                    servidor = new ServerSocket(5001);
                    System.out.println("Reparador de celulares listo, esperando peticiones");
                    conexion = servidor.accept(); 
                    System.out.println("Trabajdor listo para reparar");
                } catch (IOException ex) {
                    System.err.println(ex);
                    System.out.println("No se pudo conectar");
                }
            try {
                 entrada = new ObjectInputStream(conexion.getInputStream());
                 salida = new ObjectOutputStream(conexion.getOutputStream());
            } catch (IOException ex) {
                System.err.println(ex);
               System.out.println("Falla en conexion");
            }
                try {
                    ACLMessage msg =  (ACLMessage)entrada.readObject();
                    String opcion =  msg.getContent();
                    switch(opcion){
                        case "1":
                            cambioC();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "2":
                            display();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "3":
                            desbloquear();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "4":
                            mica();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "5":
                            plug();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                            
                    }
                } catch (Exception e) {
                     System.err.println(e);
                }
            }            
        }
        
        public void cambioC(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >4){
                enviaMensaje("Reemplazo de centro de carga exitoso, su total es de: $200");
            }else{
                enviaMensaje("Reemplazo de centro de carga fallido, solo se cobrara un porcentaje, su total es de: $50");
            }
        }
        
        public void display(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Reemplzao de display exitoso, su total es de: $100");
            }else{
                enviaMensaje("Reemplzao de display imposible de realizar, solo se cobrara un porcentaje, su total es de: $20");
            }
        }
        
        public void desbloquear(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >5){
                enviaMensaje("Desbloqueo exitoso, su total es de: $600");
            }else{
                enviaMensaje("Desbloque fallido, su celular ahora es inservible, su total es de: $0");
            }
        }
        
        public void mica(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=0){
                enviaMensaje("Colocacion de mica exitoso, su total es de: $100");
            }else{
                enviaMensaje("Cambio de teclado no es posible, solo se cobrara un porcentaje, su total es de: $20");
            }
        }
        
        public void plug(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Aumento de memoria RAM exitoso, su total es de: $1500");
            }else{
                enviaMensaje("El aumento de memoria RAM no es posible, solo se cobrara un porcentaje, su total es de: $50");
            }
        }
        
        
        void enviaMensaje(String men){
            ACLMessage res = new ACLMessage(ACLMessage.INFORM);
            vnt.setTA1("Trabajando...\n");
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            res.setContent(men);
            try {
                salida.writeObject(res);
                salida.flush();
            } catch (IOException ex) {
                Logger.getLogger(Pcs.class.getName()).log(Level.SEVERE, null, ex);
            }  
            vnt.setTA1(men);
        }

        @Override
        public boolean done() {
            System.out.println("Fin");//To change body of generated methods, choose Tools | Templates.
            return true;
        }
    }
    
    @Override
    protected void setup() {
          vnt=new Ventana2();
           GraphicsConfiguration config = vnt.getGraphicsConfiguration();
            Rectangle bounds = config.getBounds();
            Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(config);

            int x = bounds.x + bounds.width - insets.right - vnt.getWidth();
            int y = bounds.y + insets.top;
            vnt.setLocation(x, y);
          vnt.setVisible(true);
         // vnt.setTA1("Hey");
          addBehaviour(new Reparar());
    }
}
