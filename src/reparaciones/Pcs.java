/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reparaciones;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Pcs extends Agent{
    Socket conexion;
    ServerSocket servidor;
    DataOutputStream salidaCliente;
    ObjectInputStream entrada;
    ObjectOutputStream salida;
    Ventana1 vnt;
    class Reparar extends SimpleBehaviour{
        @Override
        public void action(){
            
            while(true){
                 try {
                    servidor = new ServerSocket(5000);
                    System.out.println("Reparador de pc's listo, esperando peticiones");
                    conexion = servidor.accept(); 
                    System.out.println("Trabajdor listo para reparar");
                } catch (IOException ex) {
                    System.err.println(ex);
                    System.out.println("No se pudo conectar");
                }
            try {
                 entrada = new ObjectInputStream(conexion.getInputStream());
                 salida = new ObjectOutputStream(conexion.getOutputStream());
            } catch (IOException ex) {
               System.out.println("Falla en conexion");
            }
                try {
                    ACLMessage msg =  (ACLMessage)entrada.readObject();
                    String opcion = msg.getContent();
                    switch(opcion){
                        case "1":
                            formateo();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "2":
                            limpieza();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "3":
                            aumento();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "4":
                            cambioH();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                        case "5":
                            cambioT();
                            entrada.close();
                            salida.close();
                            conexion.close();
                            servidor.close();
                            break;
                            
                    }
                } catch (Exception e) {
                     System.out.println("Error");
                }
            }            
        }
        
        public void formateo(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=4){
                enviaMensaje("Formateo exitoso, su total es de: $200");
            }else{
                enviaMensaje("Formateo erroneo, solo se cobrara un porcentaje, su total es de: $50");
            }
        }
        
        public void limpieza(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Limpieza exitosa, su total es de: $100");
            }else{
                enviaMensaje("Limpieza fallida, solo se cobrara un porcentaje, su total es de: $20");
            }
        }
        
        public void cambioH(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Cambio de HDD exitoso, su total es de: $2300");
            }else{
                enviaMensaje("Cambio de HDD no es posible, solo se cobrara un porcentaje, su total es de: $50");
            }
        }
        
        public void cambioT(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Cambio de Teclado exitoso, su total es de: $300");
            }else{
                enviaMensaje("Cambio de teclado no es posible, solo se cobrara un porcentaje, su total es de: $20");
            }
        }
        
        public void aumento(){
            Random rn = new Random();
            int answer = rn.nextInt(10) + 1;
            if(answer >=3){
                enviaMensaje("Aumento de memoria RAM exitoso, su total es de: $1500");
            }else{
                enviaMensaje("El aumento de memoria RAM no es posible, solo se cobrara un porcentaje, su total es de: $50");
            }
        }
        
        
        void enviaMensaje(String men){
            ACLMessage res = new ACLMessage(ACLMessage.INFORM);
            vnt.setTA1("Trabajando...\n");
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            res.setContent(men);
            try {
                salida.writeObject(res);
                salida.flush();
            } catch (IOException ex) {
                Logger.getLogger(Pcs.class.getName()).log(Level.SEVERE, null, ex);
            }  
            vnt.setTA1(men);
        }

        @Override
        public boolean done() {
            System.out.println("Fin");//To change body of generated methods, choose Tools | Templates.
            return true;
        }
    }
    
    @Override
    protected void setup() {
          vnt=new Ventana1();
          vnt.setVisible(true);
         // vnt.setTA1("Hey");
          addBehaviour(new Reparar());
    }
}
