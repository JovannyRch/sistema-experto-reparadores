/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reparaciones;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jade.core.Runtime;
import jade.domain.introspection.ACLMessage;

public class Reparaciones {

    /**
     * @param args the command line arguments
     */
    public static Ventana1 vnt;
    public static void main(String[] args) {
         Runtime rt = Runtime.instance();
           // Create a container to host the Book Buyer agent
           Profile p = new ProfileImpl();
           p.setParameter(Profile.MAIN_HOST, "localhost");
           p.setParameter(Profile.MAIN_PORT, "1099");
           ContainerController cc = rt.createMainContainer(p);
   try {
         // AgentController co = cc.createNewAgent("controlador", "proyectoexpertosserver.control", args);
          AgentController ac = cc.createNewAgent("Tecnico1", "reparaciones.Pcs", args);
          AgentController ac1 = cc.createNewAgent("Tecnico2","reparaciones.Cels", args);
         // co.start();
          ac.start();
          ac1.start();
      } catch (StaleProxyException ex) {
          Logger.getLogger(Ventana1.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    
}
